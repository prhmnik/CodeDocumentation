const express = require('express');
const mongoose = require('mongoose');
const exphbs = require('express-handlebars');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const expressValidation  = require('express-validator');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const upash = require('upash');
const expressValidator = require('express-validator');
const expressSession = require('express-session');

// Installing Hashing Algorithm
upash.install('argon2', require('@phc/argon2'));

const app = express();

// MongoDB Connection String
const dbUrl = "mongodb://localhost:27017/CodeDocumentationDB";

//Set up template engine
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// Set up body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

// Express session
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));

//Connect flash
app.use(flash());

//Global vars for flash
app.use(function(req, res, next){
        res.locals.success_msg = req.flash('success_msg');
        res.locals.error_msg = req.flash('error_msg');
        res.locals.error = req.flash('error');
//        res.locals.author = req.author || null;
        next();
});
app.use(passport.initialize());
app.use(passport.session());
app.use(expressValidation());

// Serving static files
app.use(express.static('./public'));
app.use(expressSession({secret: 'max', saveUninitialized: false, resave: false}));

// Author Controller Module
require('./controllers/AuthorController')(app);

// Login Controller Module
require('./controllers/LoginController')(app);

// Login Controller Module
require('./controllers/CategoryController')(app);

// Home Controller Module
require("./controllers/HomeController")(app);

// Post Controller Module
require("./controllers/PostController")(app);


// The 404 Route (ALWAYS Keep this as the last route)
app.all('*', function (req, res) {
    throw 'TestError';
});

// Error handling function
app.use(function (err, req, res, next) {
    console.log('test 500');
    res.status(500).render("404", {title: "خطا!", layout: false});
    //res.redirect("/404");
});

app.use(function (req, res, next) {
    console.log('test 404');
    res.status(404).render("404", {title: "خطا!", layout: false});
});

// Connecting to Mongoose
mongoose.connect(dbUrl, {useNewUrlParser: true}, (err) => {
    if (err)
        console.log("Unable to connect to the mongodb", err);
    else
        console.log("Connected to MongoDB")

})

// Initialization Server
app.listen(3000, (err) => {

    if (err) {
        console.log(`An Error happend : ${err.message}`);
    }

    console.log('Server is listening on port : http://localhost:3000');

});
