const mongoose = require('mongoose');

let PostSchema = mongoose.Schema({
    title:{ 
        type: String,
        required: true
    },
    text:{ 
        type: String,
    },
    order:{ 
        type: Number,
        required: true
    },
    date:{ 
        type: Date,
        default: Date.now,
        required: true
    },
    authorId:{ 
        type: Number,
        required: true
    },
    categoryId:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Category'
    }
});

module.exports = mongoose.model('Post', PostSchema);
