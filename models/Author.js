const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

let AuthorSchema = mongoose.Schema({
        username:{
            type: String,
            trim: true,
            required: true,
            index: true
        },
        password:{
            type: String,
            required: true
        }
});

//module.exports = mongoose.model('Author', AuthorSchema);

let author = module.exports = mongoose.model('author', AuthorSchema);


module.exports.createAuthor = function (newAuthor, callback) {
    bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(newAuthor.password, salt, function(err, hash) {
                newAuthor.password = hash;
                newAuthor.save(callback);
        });
    });
}

module.exports.getAuthorByAuthorname = function(username, callback) {
    let query = {username: username};
    author.findOne(query, callback);
}

module.exports.getAuthorById = function(id, callback) {
    author.findById(id, callback);
}

module.exports.comparePassword = function(candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
            if(err) throw err;
            callback(null, isMatch);
    });
}
