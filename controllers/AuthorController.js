const Author = require('../models/Author');
const bcrypt = require('bcryptjs');

module.exports = function (app) {

    app.get('/author/list', ensureAuthenticated, function (req, res) {

        Author.find({}, function (err, authors) {
            if (err) {
                console.log(err.message);
                res.render('author/list', {
                    message: {
                        success: false,
                        text: "خطا در ارتباط با سرور"
                    },
                    title: "نویسنده ها | لیست",
                    authors: []
                });
            }

            res.render('author/list', {authors: authors, title: "نویسنده ها | لیست"});
        });

    });

    //Edit author
    app.get('/author/edit/:id', ensureAuthenticated, function (req, res) {
        Author.findById(req.params.id, function (err, author) {
            if (err) {
                console.log(err.message);
                res.render('author/edit', {
                    message: {
                        success: false,
                        text: "خطا در ارتباط با سرور"
                    },
                    title: "نویسنده ها | ویرایش",
                    author: {}
                });
            }
            res.render('author/edit', {author: author, title: "نویسنده ها | ویرایش"});
        });

    });

    //Update author
    app.post('/author/edit', function (req, res) {
        let author = {};
        author.username = req.body.username;
        author.password = req.body.password;
        let newPassword = req.body.newPassword;

        let query = {_id: req.body.id};

        if (newPassword && newPassword !== "") {
			bcrypt.genSalt(10, function(err, salt) {
				bcrypt.hash(newPassword, salt, function(err, hash) {
						author.password = hash;
                        Author.updateOne(query, author, function (err) {
                            if (err) {
                                console.log(err.message);
                                author.password = newPassword;
                                res.render('author/edit/' + author.id, {
                                    message: {
                                        success: false,
                                        text: "خطا در ارتباط با سرور"
                                    },
                                    title: "نویسنده | ویرایش",
                                    author: author
                                });
                            } else {
                                req.flash('success_msg', 'یک کاربر با موفقیت ثبت شد.');
                                res.redirect('/author/list');
                            }
                        });
				});
		    });
        }
    });


    //Delete author
    app.get('/author/delete/:id', ensureAuthenticated, function (req, res) {
        let id = req.params.id;

        Author.findById(id).deleteOne(function (err) {
            if (err) {
                console.log(err.message);
                res.json({
                    success: false,
                    text: "خطا در هنگام حذف کردن"
                });
            } else {
                res.json({
                    success: true,
                    text: "با موفقیت حذف شد"
                });
            }
        });
    });

    app.get('/author/add', ensureAuthenticated, function (req, res, next) {
        res.render('author/add', {success: req.session.success, errors: req.session.errors});
        req.session.errors = null;
    });

    app.post('/author/add', function (req, res) {

        let username = req.body.username;
        let password = req.body.password;
        //Validation
        //
        req.checkBody('username', 'نام کاربری الزامیست.').notEmpty();
        req.checkBody('password', 'رمز عبور الزامیست.').notEmpty();
        req.checkBody('password', 'رمز عبور نا معتبر است.').isLength({min: 6});

        let errors = req.validationErrors();

        if(errors) {
            res.render('author/add', {
                    errors: errors,
                });
        } else {
            Author.findOne({username: req.body.username}, async function (err, existedAuthor) {
                    if (err) {
                        console.log(err.message);
                        res.render('author/add', {
                            message: {
                                success: false,
                                text: "خطا در ارتباط با سرور"
                            },
                            title: "نویسنده ها | افزودن"
                        });
                    }
                    else if (!err && existedAuthor) {
                        res.render('author/add', {
                            message: {
                                success: false,
                                text: "نویسنده ای با این نام کاربری از قبل ثبت شده"
                            },
                            title: "نویسنده | افزودن"
                        });
                    }
                    else {
                        let newAuthor = new Author({
                                username: username,
                                password: password
                            });

                        Author.createAuthor(newAuthor, function(err, author){
                                if(err) throw err;
                                console.log(author);
                                });

                        req.flash('success_msg', 'یک کاربر با موفقیت ثبت شد.');
                        res.redirect('/author/list');
                    }
            });
        }
    });
    function ensureAuthenticated (req, res, next) {
        if(req.isAuthenticated()) {
            return next();
        } else {
            req.flash('error_msg', 'باید وارد سایت شوید.');
            res.redirect('/login');
        }
    }
};
