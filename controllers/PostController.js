const Post = require("../models/Post");
const Category = require("../models/Category");

module.exports = (app) => {

    // Add GET
    app.get('/post/add', ensureAuthenticated, (req, res) => {
        Category.find({}, function (err, categories) {
            if (err) {
                console.log(`En Error Occurred : 
                            ${err.message}`);
            } else {
                res.render('post/add', {
                    message: {}, categories: categories, title: "مطالب | افزودن"
                })
            }
        });

    });

    // Add POST
    app.post('/post/add', (req, res) => {
        let newPost = new Post(req.body);
        newPost.date = new Date();
        newPost.authorId = 1;

        newPost.save((postErr) => {
            console.log("saved");
            Category.find({}, function (err, categories) {
                if (err) {
                    console.log(`En Error Occurred : 
                            ${err.message}`);

                } else {
                    if (!postErr) {
                        console.log("Post Saved");

                        res.render('post/add', {
                            message: {
                                success: true,
                                text: "با موفقیت ثبت شد"
                            },
                            categories: categories,
                            title: "مطالب | افزودن"
                        });
                    }
                    else {
                        console.log(`Error During Save Post : 
                            ${postErr.message}`);
                        res.render('post/add', {
                            message: {
                                success: false,
                                text: "خطا در هنگام ثبت",
                                categories: categories
                            },
                            title: "مطالب | افزودن"
                        });
                    }
                }
            });
        });
    });

    // Edit GET
    app.get('/post/edit/:id', ensureAuthenticated, (req, res) => {
        let postId = req.params.id;
        Category.find({}, function (err, categories) {
            if (err) {
                console.log(`En Error Occurred : 
                            ${err.message}`);
            } else {

                Post.findById(postId, function (err2, post) {
                    if (err2) {
                        console.log(`En Error Occurred : 
                            ${err2.message}`);
                    } else {
                        res.render('post/edit', {
                            message: {}, categories: categories, post: post, title: "مطالب | ویرایش",
                            helpers: {
                                isSelected: function (value1) {
                                    return (value1 == post.categoryId) ? 'selected' : "";
                                }
                            }
                        })
                    }
                })


            }
        });

    });

    // Edit POST
    app.post('/post/edit', (req, res) => {
        let newPost = {
            title: req.body.title,
            categoryId: req.body.categoryId,
            order: req.body.order,
            text: req.body.text,
            authorId: req.body.authorId
        }
        let query = {_id: req.body.id};

        Post.updateOne(query, newPost, (err) => {
            if (err) {
                // TODO: Fix this part
                console.log(`Error During Updating Post :
                            ${postErr.message}`);

                newPost.id = id;
                res.render('post/edit', {
                    message: {
                        success: false,
                        text: "خطا در هنگام ویرایش",
                        categories: categories
                    },
                    title: "مطالب | ویرایش",
                    post: newPost
                });

            } else {
                res.redirect("/post/list");
            }
        });
    });

    // List GET
    app.get('/post/list', ensureAuthenticated, (req, res) => {
        Post.find({}, ['id', 'title', 'categoryId', 'order'],
            {
                sort: {
                    order: 1 //Sort by Date Added DESC
                }
            }, function (err, posts) {
                if (err) {
                    console.log("An Error Occurred : ", err.message);
                } else {
                    Category.find({}, [],
                        {
                            sort: {
                                order: 1 //Sort by Date Added DESC
                            }
                        }, function (err2, categories) {
                            if (err2) {
                                console.log("An Error Occurred : ", err2.message);
                            } else {
                                let postList = [];

                                posts.forEach(function (post) {

                                    let category = categories.filter((item) => {
                                        return (item.id == post.categoryId);
                                    });

                                    postList.push({
                                        id: post.id,
                                        title: post.title,
                                        order: post.order,
                                        category: category[0].title
                                    })
                                });


                                res.render("post/list", {posts: postList, title: "مطالب | لیست"});
                            }
                        })
                }
            })
    });

    // Delete GET
    app.get('/post/delete/:id', ensureAuthenticated, (req, res) => {
        let postId = req.params.id;
        console.log("POST ID : ", postId);
        if (postId) {
            Post.findById(postId).remove(function (err) {
                if (err) {
                    console.log(`An Error Occurred :
                                 ${err.message}`);

                    res.json({
                        success: false,
                        text: "خطا در حین حذف کردن"
                    });
                } else {
                    res.json({
                        success: true,
                        text: "با موفقیت حذف شد"
                    });
                }

            })
        }
    });
    function ensureAuthenticated (req, res, next) {
        if(req.isAuthenticated()) {
            return next();
        } else {
            req.flash('error_msg', 'باید وارد سایت شوید.');
            res.redirect('/login');
        }
    }
};
