const Post = require("../models/Post");
const Category = require("../models/Category");
const persianDate = require('persian-date');

module.exports = (app) => {

    app.get('/:id?', (req, res) => {
        let found = true;

        let postId = req.params.id;

        let requestedPost = null;

        if (postId) {
            Post.findById(postId, function (err, post) {
                requestedPost = post;
                if (err || !post) {
                    console.log(`An Error Occurred :
                                 ${err.message}`);
                    requestedPost = {};
                    found = false;
                }
            });
        }

        let newCategories = [];
        Post.find({}, [], {
            sort: {
                order: 1 //Sort by Date Added DESC
            }
        }, function (err, posts) {
            if (err) {
                console.log(`An Error Occurred : 
                                 ${err.message}`);
                posts = [];
            }

            Category.find({}, [],
                {
                    sort: {
                        order: 1 //Sort by Date Added DESC
                    }
                }, function (err2, categories) {
                    if (err2) {
                        console.log(`An Error Occurred : 
                                 ${err2.message}`);
                        categories = [];
                    }
                    categories.forEach((category) => {

                        let catPosts = posts.filter((item) => {
                            return (item.categoryId == category.id);
                        });
                        newCategories.push({
                            id: category.id,
                            title: category.title,
                            posts: catPosts
                        });
                    });


                    res.render('index', {
                        found : found,
                        layout: false,
                        title: "صفحه اصلی",
                        post: requestedPost,
                        categories: newCategories,
                        helpers: {
                            slugify: function (text) {
                                return (text);
                            },
                            fixdate: function (date) {
                                let pc = new persianDate(date);
                                return pc.format("dddd d MMMM YYYY ساعت hh:mm");
                            }
                        }
                    });
                });


        });
    });

};
